const request = require('request');
const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('./database.sql', sqlite3.OPEN_READWRITE, (err)=>{
    if (err) {
        console.error(err.message);
    }
    console.log('Connected to the chinook database.');
})



const config ={
    API_Key : process.env.API_KEY
}

getComments = async function (vid_url, terms, pageToken = null) {
    return new Promise((resolve, reject) => {
        request({
            method: "GET",
            uri: "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&videoId="+vid_url+"&maxResults=100&order=time&textFormat=plainText&searchTerms="+terms+"&key="+config.API_Key+ (pageToken ? "&pageToken="+pageToken : ""),
            json: true
        }, function (err, resp, body) {
            if (err){
                reject(new Error(err))
            } else if (resp['statusCode'] !== 200){
                reject(new Error(resp['statusCode']))
            } else {
                resolve(body)
            }
        })
    })
}


async function get_all_comments(vid_id, terms){
    let pageToken = null;
    try {
        do{

            let comments = await getComments(vid_id, terms, pageToken);

            pageToken = comments['nextPageToken'] || null
            for(let comment of comments['items']){
                console.log(comment['snippet']['topLevelComment']['snippet']['publishedAt'])
                await insert_comment(comment['snippet']['topLevelComment']['snippet'], vid_id, comment['snippet']['topLevelComment']['id'])
            }
        } while (pageToken)
    } catch (e) {
        console.error(e)
    }

}

function insert_comment(comment, vid, comment_id){
    return new Promise((resolve, reject) => {
        try {
            db.serialize(function () {
                let insert = db.prepare("INSERT INTO comments VALUES (?, ?, ?, ?, ?)");
                insert.run(
                    comment['textOriginal'],
                    vid,
                    comment['likeCount'],
                    comment['publishedAt'],
                    comment_id
                )
                insert.finalize()
            })
            resolve(true)
        } catch (e) {
            reject(e)
        }
    })
}


get_all_comments("AfiINIrFMRM", "black face")
    .then(result=>{
        console.log(result);
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
            console.log('Close the database connection.');
        });
    }).catch(error=>{
        console.error(error);
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
            console.log('Close the database connection.');
        });
})


